package DataParsers;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import metadata.ConceptClass;
import metadata.*;

/**
 * This class takes a local filepath to
 * http://downloads.hmpdacc.org/data/reference_genomes/body_sites/Gastrointestinal_tract.gbk
 * http://hmpdacc.org/HMRGD/
 *
 * @author Matthew Collison
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 *
 */
public class HmpGenomeParser {

    private final String institute = "HMP";
    private final String GENOME = "Genome";
    private String filepath;
    private Scanner sc;
    private String term1 = "ORGANISM";
    private String term2 = "CDS";
    private List<String> organisms = new ArrayList<String>();

    public HmpGenomeParser(String filepath) {
        this.filepath = filepath;
    }

    public void parseFile() {

        try {
            //create node for institute
            Institute ins = new Institute(institute);
            GraphHandler.addNode(ins);

            //load file into scanner 
            sc = new Scanner(new File(filepath));

            //read each line (use for loop while testing)
            while (sc.hasNextLine()) {
                String line = sc.nextLine().trim();
                if (line.startsWith("ORGANISM")) {
//                    System.out.println("new genome");
                }
                if (line.startsWith(term2)) {
                    if (!organisms.contains(line)) {
                        organisms.add(line);
//                    System.out.println("new genome");
                    }
                }
//                if (line.startsWith(term3)) {
//                    counter3++;
////                    System.out.println("new genome");
//                }

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
//        System.out.println(term1 + " occurances: " + counter1);
//        System.out.println(term2 + " occurances: " + counter2);
//        System.out.println("distinct organisms occurances: " + counter3);
//        System.out.println(term4 + " occurances: " + counter4);
//        System.out.println("Number of lines in file: " + linecounter);
    }
}