 /*
 * Copyright(c) 2014 Matthew Collison. 
 */
package DataParsers;

import com.entanglementgraph.graph.data.Edge;
import com.entanglementgraph.graph.data.Node;
import com.entanglementgraph.revlog.RevisionLogException;
import com.entanglementgraph.revlog.commands.BranchImport;
import com.entanglementgraph.revlog.commands.EdgeModification;
import com.entanglementgraph.revlog.commands.GraphOperation;
import com.entanglementgraph.revlog.commands.MergePolicy;
import com.entanglementgraph.revlog.commands.NodeModification;
import com.entanglementgraph.util.GraphConnection;
import com.entanglementgraph.util.GraphConnectionFactory;
import com.entanglementgraph.util.GraphConnectionFactoryException;
import com.entanglementgraph.util.TxnUtils;
import com.mongodb.ServerAddress;
import com.scalesinformatics.mongodb.dbobject.DbObjectMarshallerException;
import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import org.openide.util.Exceptions;

/**
 * This class handles communications with the entanglement database server.
 * Methods are called to commit build information in a concurrent way.
 *
 * @author Matthew Collison
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 *
 */
public class GraphHandler {

    private static boolean gephiVis = true;
    //database and graph information 
    private static String clustername = "local-cluster";
    private static String hostname = "localhost";
    private static String databasename = "mni_v01";
    private static String graphname = "root";
    private static String branchname = "master";
    //transaction handling variables
    private static int commitSize = 10000;
    private static GraphConnection graphConn;
    private static List<GraphOperation> graphOps = new LinkedList<GraphOperation>();
    private static String txnId;
    private static int partId;
    private static long startTime = System.currentTimeMillis();
    private static long commitTime;
    private static int counter = 0;

    public static void intialiseTransaction() {
        try {
            GraphConnectionFactory.registerNamedPool(clustername,
                    new ServerAddress(hostname));
            GraphConnectionFactory factory = new GraphConnectionFactory(
                    GraphHandler.class.getClassLoader(), clustername, databasename);
            //use connection to specify graphname and version 
            graphConn = factory.connect(graphname, branchname);
            txnId = TxnUtils.beginNewTransaction(graphConn);
            System.out.println("Sucesfully connected to graph");
            System.out.println("Graph name: " + graphConn.getGraphName());
            partId = 0;
            commitTime = System.currentTimeMillis();
            System.out.println("Connection time (milliseconds): "
                    + (commitTime - startTime));
        } catch (UnknownHostException ex) {
            Exceptions.printStackTrace(ex);
        } catch (GraphConnectionFactoryException ex) {
            Exceptions.printStackTrace(ex);
        } catch (RevisionLogException ex) {
            Exceptions.printStackTrace(ex);
        }

    }

    /**
     * Commit graph modifications
     */
    public static void commitUpdate() {
        try {
            TxnUtils.submitTxnPart(graphConn, txnId, partId, graphOps);
            TxnUtils.commitTransaction(graphConn, txnId);
            partId++;
            System.out.println(commitSize + " entities commited to graph (milliseconds): "
                    + (System.currentTimeMillis() - commitTime));
            commitTime = System.currentTimeMillis();

            counter = counter + commitSize;
            System.out.println(counter + " entities commited to graph in total (milliseconds): "
                    + (System.currentTimeMillis() - startTime));
            graphOps.clear();
        } catch (RevisionLogException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    public static Node addNode(Node node) {
        Node nodeMod = null;
        try {
            nodeMod = (node);
            graphOps.add(NodeModification.create(graphConn,
                    MergePolicy.APPEND_NEW__LEAVE_EXISTING, nodeMod));
            if (graphOps.size() > commitSize) {
                commitUpdate();
            }

        } catch (DbObjectMarshallerException ex) {
            Exceptions.printStackTrace(ex);
        }
        return nodeMod;
    }

    public static void addEdge(Edge edge) {
        try {
            //add edge to edge modification list 
            Edge edgeMod = (edge);
            graphOps.add(EdgeModification.create(graphConn,
                    MergePolicy.APPEND_NEW__LEAVE_EXISTING, edgeMod));
            if (graphOps.size() > commitSize) {
                commitUpdate();
            }
        } catch (DbObjectMarshallerException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    public static void importBranch(BranchImport branch) {
        graphOps.add(branch);
    }

    public static void finaliseTransaction() {
        try {
            TxnUtils.submitTxnPart(graphConn, txnId, partId, graphOps);
            TxnUtils.commitTransaction(graphConn, txnId);
            graphOps.clear();
            System.out.println("transaction complete.");
            counter = counter + commitSize;
            System.out.println(counter + " entities commited to graph in total (milliseconds): "
                    + (System.currentTimeMillis() - startTime));
//            TxnUtils.submitAsTxn(graphConn, graphOps);
        } catch (RevisionLogException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    public static void muteLogger() {
        LogManager.getLogManager().reset();
        Logger globalLogger = Logger.getLogger(java.util.logging.Logger.GLOBAL_LOGGER_NAME);
        globalLogger.setLevel(java.util.logging.Level.OFF);
    }

    public static void setGraphName(String str) {
        graphname = str;
    }

    public static void setDatabaseName(String str) {
        databasename = str;
    }

    public static String getHostname() {
        return hostname;
    }

    public static String getGraphname() {
        return graphname;
    }

    public static GraphConnection getGraphConn() {
        return graphConn;
    }

    public static List<GraphOperation> getGraphOps() {
        return graphOps;
    }

    public static String getClustername() {
        return clustername;
    }

    public static String getDatabasename() {
        return databasename;
    }

    public static String getBranchname() {
        return branchname;
    }

    public static int getCommitSize() {
        return commitSize;
    }
}
