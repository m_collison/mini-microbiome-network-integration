 /*
 * Copyright(c) 2014 Matthew Collison. 
 */
package DataParsers;

import com.entanglementgraph.graph.data.Node;
import java.io.File;
import java.util.Scanner;
import metadata.*;
import uk.co.mattcollison.microbiomenetworkintegration.GephiExporter;

/**
 *
 * @author Matthew Collison
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 *
 */
public class KeggOrthologyParser {

    private final String institute = "Kyoto University";
    private String filepath;
    private Scanner sc;

    public KeggOrthologyParser(String filepath) {
        this.filepath = filepath;
    }

    public void parseFile() {

        try {
            //create node for institute
            Institute ins = new Institute(institute);
            GraphHandler.addNode(ins);

            //load file into scanner 
            sc = new Scanner(new File(filepath));

            //read each line (use for loop while testing)
            Node a = null;
            Node b = null;
            Node c = null;
            Node d = null;
            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                if (line.startsWith("A")) {
                    String[] splits = line.split(">");
//                    System.out.println(splits[1].substring(0, splits[1].length() - 3));
                    a = new KOlevel1(splits[1].substring(0, splits[1].length() - 3));
                    GraphHandler.addNode(a);
                    GraphHandler.addEdge(new RelationType(
                            RelTypes.ORIGINATED_FROM, a, ins));
                }
                if (line.startsWith("B")) {
                    String[] splits = line.split(">");
                    if (splits.length != 1) {
//                        System.out.println(splits[1].substring(0, splits[1].length() - 3));
                        b = new KOlevel2(splits[1].substring(0, splits[1].length() - 3));
                        GraphHandler.addNode(b);
                        GraphHandler.addEdge(new RelationType(
                                RelTypes.ORIGINATED_FROM, b, a));
                    }
                }
                if (line.startsWith("C")) {
                    String[] splits = line.split(" +");
                    String description = "";
                    for (int i = 2; i < splits.length; i++) {
                        description = description + splits[i] + " ";
                    }
//                    System.out.println(splits[1] + "\t" + description);
                    c = new KOlevel3(splits[1], description);
                    GraphHandler.addNode(c);
                    GraphHandler.addEdge(new RelationType(
                            RelTypes.ORIGINATED_FROM, c, b));
                }
                if (line.startsWith("D")) {
                    String[] splits = line.split(" +");
                    String description = "";
                    for (int i = 2; i < splits.length; i++) {
                        description = description + splits[i];
                    }
//                    System.out.println(splits[1] + "\t" + description);
                    d = new KOlevel4(splits[1], description);
                    GraphHandler.addNode(d);
                    GraphHandler.addEdge(new RelationType(
                            RelTypes.ORIGINATED_FROM, d, c));
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
