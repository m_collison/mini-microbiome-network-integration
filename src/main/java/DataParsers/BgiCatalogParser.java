 /*
 * Copyright(c) 2014 Matthew Collison. 
 */
package DataParsers;

import com.entanglementgraph.graph.data.Node;
import java.io.File;
import java.util.Scanner;
import metadata.*;
import uk.co.mattcollison.microbiomenetworkintegration.GephiExporter;

/**
 *
 * @author Matthew Collison
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 *
 */
public class BgiCatalogParser {

    private final String institute = "BGI";
    private String filedir;
    private Scanner sc;

    public BgiCatalogParser(String filedir) {
        this.filedir = filedir;
    }

    public void parseGene2Kegg() {

        try {
            //create node for institute
            Institute ins = new Institute(institute);
            GraphHandler.addNode(ins);

            //load file into scanner 
            sc = new Scanner(new File(filedir + "Gene2KEGG.list"));

            while (sc.hasNextLine()) {
                Node gene = GraphHandler.addNode(new Gene(sc.next()));
                Node ko = GraphHandler.addNode(new KOlevel4(sc.next()));
                GraphHandler.addEdge(new RelationType(RelTypes.DATA_SOURCE, gene, ko));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void parseGene2eggNog() {
        try {

            Institute ins = new Institute(institute);
            GraphHandler.addNode(ins);

            //load file into scanner 
            sc = new Scanner(new File(filedir + "Gene2eggNOG.list"));

            //read each line (use for loop while testing)
            while (sc.hasNextLine()) {
                Node gene = GraphHandler.addNode(new Gene(sc.next()));
                Node eggNog = GraphHandler.addNode(new EggNog(sc.next()));
                GraphHandler.addEdge(new RelationType(RelTypes.DATA_SOURCE, gene, eggNog));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void parseGene2Genus() {
        try {

            Institute ins = new Institute(institute);
            GraphHandler.addNode(ins);

            //load file into scanner 
            sc = new Scanner(new File(filedir + "Gene2Genus.list"));

            //read each line (use for loop while testing)
            while (sc.hasNextLine()) {
                Node gene = GraphHandler.addNode(new Gene(sc.next()));
                Node genus = GraphHandler.addNode(new Genus(sc.next()));
                GraphHandler.addEdge(new RelationType(RelTypes.DATA_SOURCE, gene, genus));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void parseMLG() {

        try {
            //create node for institute
            Institute ins = new Institute(institute);
            GraphHandler.addNode(ins);

            //load file into scanner 
            sc = new Scanner(new File(filedir + "MLG.csv"));

            //read each line (use for loop while testing)
            while (sc.hasNextLine()) {
                String line = sc.nextLine();
//                counter++;
                String[] split = line.split("\t");
                MLG mlg = new MLG(split[0]);
                GraphHandler.addNode(mlg);
                GraphHandler.addEdge(new RelationType(RelTypes.ORIGINATED_FROM,
                        mlg, ins));
                String[] geneNums = split[2].split(",");
                for (int i = 0; i < geneNums.length; i++) {
                    Gene gene = new Gene(geneNums[i]);
                    GraphHandler.addNode(gene);
                    GraphHandler.addEdge(new RelationType(
                            RelTypes.ORIGINATED_FROM, mlg, gene));
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
//        System.out.println("Total number of MLGs: " + counter);
    }

    public void parseGene2EuropeanGeneCatalogue() {
        try {

            Institute ins = new Institute(institute);
            GraphHandler.addNode(ins);

            //load file into scanner 
            sc = new Scanner(new File(filedir + "Gene2EuropeanGeneCatalogue.list"));

            //read each line (use for loop while testing)
            while (sc.hasNextLine()) {
                Node gene = GraphHandler.addNode(new Gene(sc.next()));
                Node euroGene = GraphHandler.addNode(new Gene(sc.next()));
                GraphHandler.addEdge(new RelationType(RelTypes.IS_EQUAL, gene, euroGene));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
