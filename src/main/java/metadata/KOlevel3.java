/*
 * Copyright(c) 2014 Matthew Collison. 
 */

package metadata;

/**
 *
 * @author Matthew Collison 
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 * 
 */

public class KOlevel3 extends KO{

    public KOlevel3(String id, String name){
        super(id, name);
    }
}
