 /*
 * Copyright(c) 2014 Matthew Collison. 
 */

package metadata;

import com.scalesinformatics.util.UidGenerator;

/**
 *
 * @author Matthew Collison
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 * 
 */

public class KO extends ConceptClass {

    public KO(String id, String name){
        super(name);
        super.getKeys().addUid(id);
    }
    public KO(String id){
        super(id);
    }
    
}
