/*
 * Copyright(c) 2014 Matthew Collison. 
 */

package metadata;

/**
 *
 * @author Matthew Collison 
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 * 
 */

public class KOlevel1 extends KO{

    public KOlevel1(String id){
        super(id);
    }
}
