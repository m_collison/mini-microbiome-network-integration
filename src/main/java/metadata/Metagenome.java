 /*
 * Copyright(c) 2014 Matthew Collison. 
 */

package metadata;

/**
 *
 * @author Matthew Collison
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 * 
 */

public class Metagenome extends Evidence {

    private String phenotype;
    
    public Metagenome(String s, String phenotype){
        super(s);
        this.phenotype=phenotype;
    }
    
    public Metagenome(String s){
        super(s);
    }
    
}