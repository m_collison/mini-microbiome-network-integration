 /*
 * Copyright(c) 2014 Matthew Collison. 
 */

package metadata;

/**
 *
 * @author Matthew Collison
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 * 
 */

public class Gene extends ConceptClass {
    public Gene(String id){
        super(id);
    }

}
