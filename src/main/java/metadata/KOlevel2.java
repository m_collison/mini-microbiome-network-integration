/*
 * Copyright(c) 2014 Matthew Collison. 
 */

package metadata;

/**
 *
 * @author Matthew Collison 
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 * 
 */

public class KOlevel2 extends KO{

    public KOlevel2(String id){
        super(id);
    }
}
