 /*
 * Copyright(c) 2014 Matthew Collison. 
 */
package metadata;

import com.entanglementgraph.graph.data.Node;
import com.scalesinformatics.util.UidGenerator;

/**
 *
 * @author Matthew Collison
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 *
 */
public class Institute extends Node {

    private String fullname;

    public Institute(String id, String fullname) {
        super();
        super.getKeys().setType(this.getClass().getName());
        super.getKeys().addUid(UidGenerator.generateUid());
        super.getKeys().addName(id);
        setFullname(fullname);
    }

    public Institute(String id) {
        super();
        super.getKeys().setType(this.getClass().getName());
        super.getKeys().addUid(UidGenerator.generateUid());
        super.getKeys().addName(id);
    }

    public String getName() {
        return this.getClass().getName();
    }

    public String getFullname() {
        return this.fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }
}
