 /*
 * Copyright(c) 2014 Matthew Collison. 
 */
package metadata;

import com.entanglementgraph.graph.data.Edge;
import com.entanglementgraph.graph.data.Node;
import com.scalesinformatics.util.UidGenerator;

/**
 *
 * @author Matthew Collison
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 *
 */
public class RelationType extends Edge {

    public static int counter = 0;

    public RelationType(RelTypes reltype) {
        super();
        super.getKeys().setType(this.getClass().getName());
        super.getKeys().setType(reltype.name());
        super.getKeys().addUid(UidGenerator.generateUid());
        super.getKeys().addName(String.valueOf(counter));
        counter++;
    }

    public RelationType(RelTypes reltype, Node from, Node to) {
        super();
//        super.getKeys().setType(this.getClass().getName());
        super.getKeys().setType(reltype.name());
        super.getKeys().addUid(UidGenerator.generateUid());
        super.getKeys().addName(String.valueOf(counter));
        super.setFrom(from.getKeys());
        super.setTo(to.getKeys());
        counter++;
    }

    public RelationType(String name, RelTypes reltype) {
        super();
//        super.getKeys().setType(this.getClass().getName());
        super.getKeys().setType(reltype.name());
        super.getKeys().addUid(UidGenerator.generateUid());
        super.getKeys().addName(name);
    }

    public RelationType(String name, RelTypes reltype, Node from, Node to) {
        super();
//        super.getKeys().setType(this.getClass().getName());
        super.getKeys().setType(reltype.name());
        super.getKeys().addUid(UidGenerator.generateUid());
        super.getKeys().addName(name);
        super.setFrom(from.getKeys());
        super.setTo(to.getKeys());
    }

    public String getName() {
        return this.getClass().getName();
    }
}
