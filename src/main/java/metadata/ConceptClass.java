 /*
 * Copyright(c) 2014 Matthew Collison. 
 */

package metadata;

import com.entanglementgraph.graph.data.EntityKeys;
import com.entanglementgraph.graph.data.Node;
import com.scalesinformatics.util.UidGenerator;

/**
 *
 * @author Matthew Collison
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 * 
 */

public class ConceptClass extends Node {
    public ConceptClass(String name){
        super();
        super.getKeys().setType(this.getClass().getName());
        super.getKeys().addUid(name);
        super.getKeys().addName(name);
    }
    public String getName(){
        return this.getClass().getName();
    }
}
