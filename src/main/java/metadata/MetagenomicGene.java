 /*
 * Copyright(c) 2014 Matthew Collison. 
 */

package metadata;

/**
 *
 * @author Matthew Collison
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 * 
 */

public class MetagenomicGene extends Evidence {

    public MetagenomicGene(String id){
        super(id);
    }
    
    public MetagenomicGene(String id, String fasta){
        //must do something with fasta attribute
        super(id);
    }
}
