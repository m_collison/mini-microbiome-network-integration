/*
 * Copyright(c) 2014 Matthew Collison. 
 */

package metadata;

/**
 *
 * @author Matthew Collison 
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 * 
 */

public class KOlevel4 extends KO{

    public KOlevel4(String id, String name){
        super(id, name);
    }
    public KOlevel4(String id){
        super(id);
    }
    
}
