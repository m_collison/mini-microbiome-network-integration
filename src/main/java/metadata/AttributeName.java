 /*
 * Copyright(c) 2014 Matthew Collison. 
 */

package metadata;

import com.entanglementgraph.graph.data.Node;
import com.scalesinformatics.util.UidGenerator;

/**
 *
 * @author Matthew Collison
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 * 
 */

public class AttributeName extends Node {
   
    private String desription;
    
    public AttributeName(String id, String description){
        super();
        super.getKeys().setType(this.getClass().getName());
        super.getKeys().addUid(UidGenerator.generateUid());
        super.getKeys().addName(id);
        setDescription(description);
    }
    
    public String getName(){
        return this.getClass().getName();
    }
    
    public String getDescription(){
        return this.desription;
    }
    
    public void setDescription(String description){
        this.desription=description;
    }

}
