 /*
 * Copyright(c) 2014 Matthew Collison. 
 */

package metadata;

import com.entanglementgraph.graph.data.Node;
import com.scalesinformatics.util.UidGenerator;

/**
 *
 * @author Matthew Collison
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 * 
 */

public class Units extends Node {
    public Units(String name){
        super();
        super.getKeys().setType(this.getClass().getName());
        super.getKeys().addUid(UidGenerator.generateUid());
        super.getKeys().addName(name);
    }
    public String getName(){
        return this.getClass().getName();
    }
}
