/*
 * Copyright(c) 2014 Matthew Collison. 
 */

package metadata;

/**
 *
 * @author Matthew Collison 
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 * 
 */

public class EggNog extends ConceptClass {
    public EggNog(String id){
        super(id);
    }   
}
