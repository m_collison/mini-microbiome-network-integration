/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

//xml imports
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import metadata.*;


/**
 *
 * @author a6034850
 */
public class GoParser {

    private String TERM_TAG_NAME = "term";
    private String ID_TAG_NAME = "id";
    private String NAME_TAG_NAME = "name";
    private String DEF_TAG_NAME = "def";
    private String DEFSTR_TAG_NAME = "defstr";
    private String IS_ROOT_TAG_NAME = "is_root";
    private String IS_OBSOLETE_TAG_NAME = "is_obsolete";
    private String COMMENT_TAG_NAME = "comment";
    private String ALT_ID_TAG_NAME = "alt_id";
    private String NAMESPACE_TAG_NAME = "namespace";
    private String ISA_TAG_NAME = "is_a";
    private String RELATIONSHIP_TAG_NAME = "relationship";
    private String RELATIONSHIP_TYPE_TAG_NAME = "type";
    private String RELATIONSHIP_TO_TAG_NAME = "to";

    public GoParser(String DB_PATH, String filePath) {
    }

    private void parseGO_XML(String filePath) {

        // code generated following instructions template from http://www.mkyong.com/java/how-to-read-xml-file-in-java-sax-parser/ 

            try {
                SAXParserFactory factory = SAXParserFactory.newInstance();
                SAXParser saxParser = factory.newSAXParser();

                DefaultHandler handler = new DefaultHandler() {

                    boolean bterm = false;
                    boolean bid = false;
                    boolean bname = false;
                    boolean baltid = false;
                    boolean bisa = false;

                @Override
                    public void startElement(String uri, String localName, String qName,
                            Attributes attributes) throws SAXException {

//                    System.out.println("Start Element :" + qName);

                        if (qName.equalsIgnoreCase(TERM_TAG_NAME)) {
                            bterm = true;
                        }

                        if (qName.equalsIgnoreCase(ID_TAG_NAME)) {
                            bid = true;
                        }
                        
                        if (qName.equalsIgnoreCase(NAME_TAG_NAME)) {
                            bname = true;
                        }

                        if (qName.equalsIgnoreCase(ALT_ID_TAG_NAME)) {
                            baltid = true;
                        }

                        if (qName.equalsIgnoreCase(ISA_TAG_NAME)) {
                            bisa = true;
                        }
                    }

                    public void endElement(String uri, String localName,
                            String qName) throws SAXException {
//                    System.out.println("End Element :" + qName);
                    }

                    public void characters(char ch[], int start, int length) throws SAXException {

                        if (bterm) {
                            String s = new String(ch, start, length);
//                            System.out.println("Term Name : " + new String(ch, start, length));
                            bterm = false;
                        }

                        if (bid) {
                            String s = new String(ch, start, length);
//                            System.out.println("ID Name : " + new String(ch, start, length));
                            bid = false;
//                            System.out.println(s);
//                            id = checkNode(TERM_TAG_NAME, s);
                        }
                        
                        if (bname) {
                            String s = new String(ch, start, length);
//                            System.out.println("ID Name : " + new String(ch, start, length));
                            bname = false;
//                            System.out.println(s);

                            //KO.setProperty
//                            id.setProperty(NAME_TAG_NAME, s);
                        }

                        if (baltid) {
                            String s = new String(ch, start, length);
//                            System.out.println("Alt ID Name : " + new String(ch, start, length));
                            baltid = false;
//                            System.out.println(s);
//                            GraphHandler.addNode(new KO(s));
//                            Node altid = checkNode(TERM_TAG_NAME, s);
//                            if (id != null) {
//                                altid.createRelationshipTo(id, RelTypes.GO_ALT_ID);
//                            }
                        }

                        if (bisa) {
                            String s = new String(ch, start, length);
//                            System.out.println("Is a Name : " + new String(ch, start, length));
                            bisa = false;
//                            System.out.println(s);
//                            GraphHandler.addNode(new KO(s));
                        }
                    }
                };
                saxParser.parse(filePath, handler);
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
}
