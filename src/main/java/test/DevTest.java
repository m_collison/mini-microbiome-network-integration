 /*
 * Copyright(c) 2014 Matthew Collison. 
 */
package test;

import DataParsers.GraphHandler;
import com.entanglementgraph.graph.data.Edge;
import com.entanglementgraph.graph.data.Node;
import com.entanglementgraph.revlog.commands.BranchImport;
import com.entanglementgraph.revlog.commands.EdgeModification;
import com.entanglementgraph.revlog.commands.MergePolicy;
import com.entanglementgraph.revlog.commands.NodeModification;
import com.entanglementgraph.util.TxnUtils;
import com.scalesinformatics.util.UidGenerator;
import uk.co.mattcollison.microbiomenetworkintegration.GephiExporter;

/**
 *
 * @author Matthew Collison
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 *
 */
public class DevTest {

    public static void main(String[] args) {
        GraphHandler.muteLogger();
        GraphHandler.setDatabaseName("devtest4");
        createGraphOne();
        createGraphTwo();
        integrateGraphs();
        GephiExporter.exportDatabase("hello_worlds.gexf");
    }
    public static void createGraphOne(){
        GraphHandler.setGraphName("branch1");
        GraphHandler.intialiseTransaction();
        Node node1 = new Node();
        node1.getKeys().addNames("hello");
        node1.getKeys().setType("type");
        node1.getKeys().addUid("hello");
        GraphHandler.addNode(node1);

        Node node2 = new Node();
        node2.getKeys().addNames("world1");
        node2.getKeys().setType("type");
        node2.getKeys().addUid("world1");
        GraphHandler.addNode(node2);

        Edge edge1 = new Edge();
        edge1.getKeys().addNames("Relation1");
        edge1.getKeys().setType("type");
        edge1.getKeys().addUids(UidGenerator.generateUid());
        edge1.setFrom(node1.getKeys());
        edge1.setTo(node2.getKeys());
        GraphHandler.addEdge(edge1);

        GraphHandler.finaliseTransaction();
    }
    
    public static void createGraphTwo (){
    
        GraphHandler.setGraphName("branch2");
        GraphHandler.intialiseTransaction();
        Node node11 = new Node();
        node11.getKeys().addNames("hello");
        node11.getKeys().setType("type");
        node11.getKeys().addUid("hello");
        GraphHandler.addNode(node11);

        Node node12 = new Node();
        node12.getKeys().addNames("world2");
        node12.getKeys().setType("type");
        node12.getKeys().addUid("world2");
        GraphHandler.addNode(node12);

        Edge edge11 = new Edge();
        edge11.getKeys().addNames("Relation2");
        edge11.getKeys().setType("type");
        edge11.getKeys().addUids(UidGenerator.generateUid());
        edge11.setFrom(node11.getKeys());
        edge11.setTo(node12.getKeys());
        GraphHandler.addEdge(edge11);

        GraphHandler.finaliseTransaction();

    }
    
    public static void integrateGraphs(){
        GraphHandler.setGraphName("integrated");
        GraphHandler.intialiseTransaction();
        BranchImport bin1 = new BranchImport("branch1","master");
        System.out.println("branch1 import object created");
        GraphHandler.importBranch(bin1);
        System.out.println("branch1 object added to transaction list");
        GraphHandler.commitUpdate();
        System.out.println("transaction list with branch1 commited");
        BranchImport bin2 = new BranchImport("branch2","master");
        System.out.println("b2 created");
        GraphHandler.importBranch(bin2);
        System.out.println("b2 added to op list");
        GraphHandler.commitUpdate();
        System.out.println("b2 commited");
        GraphHandler.finaliseTransaction();
    }
}
