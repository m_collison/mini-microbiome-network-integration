 /*
 * Copyright(c) 2014 Matthew Collison. 
 */

package test;

import DataParsers.GraphHandler;
import DataParsers.KeggOrthologyParser;
import uk.co.mattcollison.microbiomenetworkintegration.GephiExporter;

/**
 *
 * @author Matthew Collison
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 * 
 */

public class KoDevTest {
    public static void main(String[] args){
        System.out.println(GraphHandler.getGraphname());
        GraphHandler.muteLogger();
        GraphHandler.setGraphName("KO");//KeggOrthology
        GraphHandler.intialiseTransaction();
        KeggOrthologyParser keggparser = new KeggOrthologyParser("D://mni_files/ko00001.keg");
        keggparser.parseFile();
        GraphHandler.finaliseTransaction();
        GephiExporter.exportDatabase("kegg_sample.gexf");
    }
}
