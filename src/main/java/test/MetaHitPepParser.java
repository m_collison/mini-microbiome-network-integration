 /*
 * Copyright(c) 2014 Matthew Collison. 
 */
package test;

import DataParsers.GraphHandler;
import java.io.File;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import metadata.ConceptClass;
import metadata.*;

/**
 * This class takes a local filepath to
 * ftp://public.genomics.org.cn/BGI/gutmeta/UniSet/UniGene.pep.gz parses the
 * data and generates a series of graph modification commands to populate an
 * entanglement graph. Currently written for the gene catalogue predicted
 * protein data.
 *
 * @author Matthew Collison
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 *
 */
public class MetaHitPepParser {

    private final String institute = "MetaHIT";
    private final String genecat = "Gene Catalogue";
    private String filepath;
    private Scanner sc;

    public MetaHitPepParser(String filepath) {
        this.filepath = filepath;
    }

    
    public void parseFile() {

        try {
            //create node for institute
            Institute ins = new Institute(institute);

            //create node for data source and link to institute 
            ConceptClass genecatalogue = new ConceptClass(genecat);
            RelationType rel = new RelationType(RelTypes.DATA_SOURCE);
            rel.setFrom(genecatalogue.getKeys());
            rel.setTo(ins.getKeys());
            GraphHandler.addEdge(rel);

            //load file into scanner 
            sc = new Scanner(new File(filepath));
            
            //skip the first line with column headings 
            String[] columns = sc.nextLine().split(" +");

            System.out.println("titles: " + columns[0] + " ");// + columns[1] + " " + columns[2] + " " + columns[3]);

            //read each line (use for loop while testing)
            while (sc.hasNextLine()) {
//            for (int i = 0; i < 5; i++) {
                String line = sc.nextLine();
                columns = line.split(" +");

                //create gene node from each line 
                Gene gene = new Gene(columns[2]);
                System.out.println(columns[2]);
                GraphHandler.addNode(gene);
                GraphHandler.addEdge(new RelationType(RelTypes.ORIGINATED_FROM, gene, genecatalogue));
                //create nodes for each property or annotation and link to gene
//                KO ko = new KO(columns[5]);
//                GraphHandler.addNode(ko);
//                GraphHandler.addEdge(new RelationType(RelTypes.DATABASE, GraphHandler.addNode(new Institute("KEGG")), ko));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
