 /*
 * Copyright(c) 2014 Matthew Collison. 
 */

package test;

import DataParsers.HmpGenomeParser;

/**
 *
 * @author Matthew Collison
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 * 
 */

public class HmpGenomeDevTest {

    public static void main(String[] args){
        HmpGenomeParser parser = new HmpGenomeParser("C://Users/Matt2/Downloads/Gastrointestinal_tract.gbk");
        parser.parseFile();
    }
}
