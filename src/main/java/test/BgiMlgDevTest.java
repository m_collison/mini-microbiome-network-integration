 /*
 * Copyright(c) 2014 Matthew Collison. 
 */

package test;

import DataParsers.*;

/**
 *
 * @author Matthew Collison
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 * 
 */

public class BgiMlgDevTest {
    public static void main(String[] args){
        GraphHandler.intialiseTransaction();
        BgiCatalogParser parser = new BgiCatalogParser("D://mni_files/");
        parser.parseGene2Kegg();
    }
}
