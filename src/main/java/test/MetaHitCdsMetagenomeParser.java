 /*
 * Copyright(c) 2014 Matthew Collison. 
 */
package test;

import DataParsers.GraphHandler;
import java.io.File;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import metadata.ConceptClass;
import metadata.*;

/**
 * This class takes a local filepath to
 * ftp://public.genomics.org.cn/BGI/gutmeta/SingleSample_GenePrediction/ parses
 * the data and generates a series of graph modification commands to populate an
 * entanglement graph. Currently written for the gene catalogue predicted
 * protein data.
 *
 * @author Matthew Collison
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 *
 */
public class MetaHitCdsMetagenomeParser {

    private final String institute = "MetaHIT";
    private final String KEGGINST = "KEGG";
    private final String genecat = "Gene Catalogue";
    private String filepath;
    private Scanner sc;

    public MetaHitCdsMetagenomeParser(String filepath) {
        this.filepath = filepath;
    }

    public void parseFile() {

        try {
            //create node for institute
            Institute ins = new Institute(institute);
            GraphHandler.addNode(ins);

            //create node for data source and link to institute 
            Metagenome metagenome = new Metagenome(filepath);
            GraphHandler.addNode(metagenome);

            //link institute to metagenome
            RelationType rel = new RelationType(RelTypes.DATA_SOURCE);
            rel.setFrom(metagenome.getKeys());
            rel.setTo(ins.getKeys());
            GraphHandler.addEdge(rel);

            //load file into scanner 
            sc = new Scanner(new File(filepath));

            //read each line (use for loop while testing)
            String fasta = "";
            String id = "";
            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                if (line.startsWith(">")) {
                    if (!id.equals("")) {
                        MetagenomicGene cds = new MetagenomicGene(id, fasta);
                        GraphHandler.addNode(cds);
                        GraphHandler.addEdge(new RelationType(RelTypes.ORIGINATED_FROM, cds, metagenome));
                        id = line.substring(1, 10);
                        fasta = "";
                    }
                } else {
                    fasta = fasta + line;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}