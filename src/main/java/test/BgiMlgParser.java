 /*
 * Copyright(c) 2014 Matthew Collison. 
 */
package test;

import DataParsers.GraphHandler;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import metadata.*;
import uk.co.mattcollison.microbiomenetworkintegration.GephiExporter;

/**
 * ftp://climb.genomics.cn/pub/10.5524/100001_101000/100036/MLG/MLG
 *
 * @author Matthew Collison
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 *
 */
public class BgiMlgParser {

    private final String institute = "BGI";
    private String filepath;
    private Scanner sc;
    private int counter = 0;

    /*
     * main method only used for testing 
     */
//    public static void main(String[] args){
//        System.out.println(GraphHandler.getGraphname());
//        GraphHandler.muteLogger();
//        GraphHandler.setGraphName("mlgss");
//        GraphHandler.intialiseTransaction();
//        BgiMlgParser mlgparser = new BgiMlgParser("D://MLG.short.sample.txt");
//        mlgparser.parseFile();
//        GraphHandler.finaliseTransaction();
//        GephiExporter.exportDatabase("mlg_sample.gexf");
//    }
    
    public BgiMlgParser(String filepath) {
        this.filepath = filepath;
    }

    public void parseFile() {

        try {
            //create node for institute
            Institute ins = new Institute(institute);
            GraphHandler.addNode(ins);

            //load file into scanner 
            sc = new Scanner(new File(filepath));

            //read each line (use for loop while testing)
            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                counter++;
                String[] split = line.split("\t");
                MLG mlg = new MLG(split[0]);
                GraphHandler.addNode(mlg);
                GraphHandler.addEdge(new RelationType(RelTypes.ORIGINATED_FROM, 
                        mlg, ins));
                String[] geneNums = split[2].split(",");
                for (int i = 0; i < geneNums.length; i++) {
                    Gene gene = new Gene(geneNums[i]);
                    GraphHandler.addNode(gene);
                    GraphHandler.addEdge(new RelationType(
                            RelTypes.ORIGINATED_FROM, mlg,gene));
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        System.out.println("Total number of MLGs: " + counter);
    }
}