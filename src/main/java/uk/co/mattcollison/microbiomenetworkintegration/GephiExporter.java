/*
 * Copyright(c) 2014 Matthew Collison. 
 */
package uk.co.mattcollison.microbiomenetworkintegration;

import DataParsers.GraphHandler;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import it.uniroma1.dis.wsngroup.gexf4j.core.Gexf;
import it.uniroma1.dis.wsngroup.gexf4j.core.Graph;
import it.uniroma1.dis.wsngroup.gexf4j.core.Node;
import it.uniroma1.dis.wsngroup.gexf4j.core.impl.GexfImpl;
import it.uniroma1.dis.wsngroup.gexf4j.core.impl.StaxGraphWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import org.openide.util.Exceptions;

/**
 *
 * @author Matthew Collison
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 *
 */
public class GephiExporter {

    private static final int MAX_NODES = 30000;
    private static final int MAX_EDGES = 30000;
    private static Gexf gexf = new GexfImpl();
    private static Graph graph = gexf.getGraph();
    private static Map<String, Object> map = new HashMap<String, Object>();

    public static void exportDatabase(String filename) {

        itterateDatabaseObjects();
        writeGexf(filename);

    }

    public static void exportMetadata(String filename) {
    }

    private static void itterateDatabaseObjects() {
        //connect to database and itterate 
        MongoClient mongoclient;

        try {
            mongoclient = new MongoClient(GraphHandler.getHostname());

            DB db = mongoclient.getDB(GraphHandler.getDatabasename());

            DBCollection coll = db.getCollection(
                    GraphHandler.getGraphname() + "_"
                    + GraphHandler.getBranchname() + "_edges");

            System.out.println(GraphHandler.getGraphname() + "_"
                    + GraphHandler.getBranchname() + "_edges");
            System.out.println("collection count: " + coll.getCount());
            if (coll.getCount() < MAX_EDGES) {
                System.out.println("check point");

                DBCursor cursor = coll.find();
                try {
                    while (cursor.hasNext()) {
                        DBObject dbobj = cursor.next();
                        DBObject subdbobjFrom = (DBObject) dbobj.get("from");
                        Node from = graph.createNode(subdbobjFrom.get("names").toString());
                        from.setLabel(subdbobjFrom.get("names").toString());
                        DBObject subdbobjTo = (DBObject) dbobj.get("to");
                        Node to = graph.createNode(subdbobjTo.get("names").toString());
                        to.setLabel(subdbobjTo.get("names").toString());
                        from.connectTo(to);
                    }
                } finally {
                    cursor.close();
                }
            }
        } catch (UnknownHostException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    private static void writeGexf(String filename) {
        StaxGraphWriter graphWriter = new StaxGraphWriter();
        File f = new File(filename);
        Writer out;
        try {
            out = new FileWriter(f, false);
            graphWriter.writeToStream(gexf, out, "UTF-8");
            System.out.println(f.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
