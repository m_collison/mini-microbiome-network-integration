package uk.co.mattcollison.microbiomenetworkintegration;

import DataParsers.*;


/**
 *
 *
 */
public class App {

    private static final String fileDir = "/home/a6034850/china_dataset/";

    public static void main(String[] args) {

        GephiExporter.exportDatabase("genecat2kegg.gexf");

//        GraphHandler.setGraphName("KO");
//        GraphHandler.muteLogger();
//        GraphHandler.intialiseTransaction();
//        KeggOrthologyParser keggParser = new KeggOrthologyParser(fileDir + "ko00001.keg");
//        keggParser.parseFile();
//        System.out.println("KEGG orthology fully parsed into branch: "
//                + GraphHandler.getBranchname());
//        GraphHandler.finaliseTransaction();
//

        GraphHandler.setGraphName("gene_catalog");
        GraphHandler.muteLogger();
        GraphHandler.intialiseTransaction();
        BgiCatalogParser genecat = new BgiCatalogParser(fileDir);
        genecat.parseGene2Genus();
        System.out.println("Gene2Genus done");
        genecat.parseGene2EuropeanGeneCatalogue();
        genecat.parseGene2Kegg();
        genecat.parseGene2eggNog();
        genecat.parseMLG();
        System.out.println("Gene catalog fully parsed into branch: "
                + GraphHandler.getBranchname());
        GraphHandler.finaliseTransaction();


        GraphHandler.finaliseTransaction();
        GephiExporter.exportDatabase("genecat2kegg.gexf");

//        GraphHandler.setGraphName("HMP_genomes");
//        HmpGenomeParser hmpGenParser = new HmpGenomeParser("Gastrointestinal_tract.gbk");
//        hmpGenParser.parseFile();
//        System.out.println("HMP genomes fully parsed into branch: "
//                + GraphHandler.getBranchname());


//        MetaHitPepParser parser = new MetaHitPepParser("./UniGene.annotation.sample");
//        parser.parseFile();

//        GraphHandler.setGraphName("MLG");
//        BgiMlgParser mlgParser = new BgiMlgParser(fileDir + "MLG.csv");
//        mlgParser.parseFile();
//        System.out.println("MLG groups fully parsed into branch: " + 
//                GraphHandler.getBranchname());

    }
}
